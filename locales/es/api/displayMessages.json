{
	"httpMethodNotAllowed": "El método HTTP \"{{method}}\" no está permitido en este endpoint",
	"endpointNotFound": "Endpoint no encontrado",
	"errorOccured": "Ha ocurrido un error",
	"missingParameter": "Falta {{parameter}}",
	"invalidParameter": "{{parameter}} es inválido",
	"invalidParameterType": "{{parameter}} debe ser un {{type}}",
	"parameterTooLong": "{{parameter}} no puede ser mayor que {{maxLen}} caracteres",
	"parameterTooShort": "{{parameter}} debe tener al menos {{minLen}} caracteres",
	"parameterNotBetween": "{{parameter}} debe estar entre {{min}} y {{max}}",
	"discordAccountNotLinked": "Una cuenta de Discord tiene que estar vinculada",
	"notLoggedIn": "Tienes que haber iniciado sesión para usar este endpoint",
	"alreadyLoggedIn": "Ya has iniciado sesión",
	"invalidUsernameCharacters": "El nombre de usuario sólo puede contener caracteres alfanuméricos ingleses",
	"usernameTaken": "El nombre de usuario ya está cogido",
	"passwordMismatch": "Las contraseñas no coinciden",
	"changePasswordSuccess": "Contraseña cambiada con éxito",
	"resourceNotFound": "Un {{resource}} con ese {{property}} no pudo ser encontrado",
	"resourceExists": "Un {{resource}} con este {{property}} ya existe",
	"premiumFeature": "Se requiere una suscripción mejorada para usar {{feature}}",
	"userNotFound": "No se pudo encontrar ese usuario",
	"fileTooLarge": "Archivo muy grande. El tamáño máximo de archivo es {{size}}",
	"invalidFileFormat": "Formato de archivo inválido",
	"bodyTooLarge": "El cuerpo proporcionado es demasiado grande",
	"bodyMalformed": "El cuerpo proporcionado está malformado",
	"authenticationApi": {
		"accountBanned": "Tu cuenta ha sido baneada. Razón: {{reason}}",
		"invalidForgotPasswordToken": "El token de tu contraseña olvidada no es válido o ha expirado",
		"passwordResetLinkSent": "Se ha intentado enviar un enlace para restablecer tu contraseña a tus MDs de Discord. Este enlace expirará en 24 horas.",
		"suspectedMultiAccounting": "Parece que podrías tener varias cuentas. Please contact upload.systems support if you believe this is a mistake.",
		"noReasonProvided": "No se proporcionó ninguna razón",
		"invalidInviteCode": "El código de invitación no existe o ya ha sido usado",
		"refreshTokenMismatch": "El refresh token no coincidió con el token proporcionado",
		"passwordResetRequired": "Tienes que restablecer tu contraseña",
		"notBetaUser": "Tienes que ser un usuario con acceso a la beta para acceder a esta página",
		"mfaCodeRequired": "Se requiere el código MFA",
		"failedCaptcha": "Captcha fallido, inténtelo de nuevo"
	},
	"collectionsApi": {
		"invalidPublicityType": "La visibilidad tiene que ser \"pública\" o \"privada\"",
		"collectionNotEmpty": "La colección tiene que estar vacía antes de eliminarla"
	},
	"discordApi": {
		"accountTooYoung": "Tu cuenta de Discord es demasiado reciente para vincularla",
		"accountNotFound": "No se pudo autenticar usando Discord. Ninguna cuenta está asociada con esa cuenta de Discord"
	},
	"domainGroupsApi": {
		"usersMustBeArray": "Los usuarios tienen que ser una matriz de UUIDs",
		"noValidUsers": "Los usuarios tienen que contener por lo menos 1 UUID de usuario válido",
		"invalidUserIds": "Se ha proporcionado un(os) ID(s) de usuario inválido(s)",
		"groupInUse": "Este grupo de acceso lo utiliza actualmente un dominio, configúralos para que usen otro grupo antes de eliminarlo"
	},
	"domainsApi": {
		"invalidTld": "No estamos aceptando dominios con este TLD, lo sentimos",
		"deletedDomain": "Dominio eliminado con éxito"
	},
	"imagesApi": {
		"futureTimestamp": "expiresAt tiene que ser una marca de tiempo en el futuro",
		"unauthenticated": "Tienes que estar autenticado para usar este endpoint",
		"invalidUploadKey": "Clave de subida inválida. ¿Has generado una clave?",
		"subscriptionMaxFileSize": "El archivo excede el tamaño máximo de tu suscripción de {{size}}",
		"subscriptionMaxStorage": "Has alcanzado tu límite de almacenamiento. Elimine algunos archivos o considere mejorar su suscripción.",
		"missingUploadPreferences": "No se ha encontrado el dominio, establece tus preferencias de subida en la página de configuración",
		"userBanned": "Actualmente estás baneado, contacta upload.systems con el soporte si crees que es un error",
		"unrecognisedIp": "Dirección IP no reconocida. Inicia sesión en la página y vuelve a intentarlo.",
		"missingExtension": "El archivo tiene que tener una extensión",
		"premiumFileType": "Se requiere una suscripción mejorada para subir este tipo de archivo",
		"spamUploadBan": "Has sido baneado por subir imágenes muy rápido, contacta upload.systems al soporte para apelar",
		"noDeletePermissions": "No tienes permiso para eliminar las imágenes de otros usuarios",
		"wipeInProgress": "Tus imágenes ya están en el proceso de ser borradas ({{percentage}}% completado)",
		"noImagesInCollection": "No hay imágenes dentro de la colección proporcionada",
		"noValidImageIds": "No se han proporcionado IDs de imágenes válidos",
		"noImagesTo": "No tienes imágenes para {{action}}",
		"archiveInProgress": "Ya tienes un archivo siendo procesado",
		"archiveAlreadyRequested": "Ya has solicitado un archivo en los últimos 7 días ",
		"reportOwnImage": "No puedes denunciar tus propias imagenes",
		"imageAlreadyReported": "Ya has denunciado esta imagen",
		"cannotSetFileAsShowcase": "Solo puedes exponer imágenes o videos"
	},
	"mailAccountsApi": {
		"invalidDomain": "Dominio inválido en el correo electrónico ",
		"maxAccounts": "Has alcanzado el número máximo de cuentas de correo que puedes tener al mismo tiempo",
		"maxAliases": "Has alcanzado el número máximo de alias de correo que puedes tener al mismo tiempo",
		"emailInUse": "La dirección de correo electrónico ya está en uso",
		"aliasExists": "Un alias con esa dirección de correo electrónico ya existe",
		"invalidCharacters": "Tienes caracteres inválidos en tu dirección de correo electrónico"
	},
	"notificationsApi": {
		"alreadyMarkedAsRead": "Esta notificación ya está marcada como leída"
	},
	"pastesApi": {
		"unsupportedLang": "El lenguaje de resaltado de sintaxis proporcionado no es compatible",
		"unsupportedType": "El tipo de paste proporcionado no es compatible",
		"privateAndPasswordProtected": "Un paste no puede ser privado y estar protegido con contraseña a la vez",
		"successfullyWiped": "{{count}} pastes borrados con éxito",
		"passwordRequired": "Este paste requiere una contraseña para descifrar",
		"incorrectPassword": "Contraseña incorrecta",
		"notOwnPaste": "Sólo puedes eliminar tus propios pastes"
	},
	"shortenUrlApi": {
		"invalidProtocol": "Protocolo de URL inválido. Tienes que usar HTTP o HTTPS",
		"invalidIDCharacters": "El ID solo puede contener caracteres ingleses y numéricos",
		"selfRedirect": "La URL no puede redirigirse a sí misma",
		"successfullyWiped": " {{count}} URLs acortados han sido borrados con éxito",
		"notOwnShortenedUrl": "Sólo puedes eliminar tus propios URLs acortados"
	},
	"subscriptionsApi": {
		"notSelected": "No has seleccionado un grupo de suscripción",
		"activeSubscription": "Ya tienes una suscripción activa",
		"groupMismatch": "El usuario proporcionado está actualmente suscrito al plan {{groupName}}, por lo que no se le puede regalar este plan",
		"selfGift": "No puedes regalarte una suscripción a ti mismo, agrega meses en tu página de gestión de suscripciones en su lugar",
		"userNotGiftable": "No puedes regalar una suscripción a este usuario en este momento, pídele que inicie sesión primero ",
		"noSubscriptionAddMonths": "No tienes una suscripción activa para añadirla meses",
		"noSubscriptionCancel": "No tienes una suscripción activa para cancelar",
		"noSubscriptionResume": "No tienes una suscripción activa para reanudar",
		"pendingPlanChange": "Ya tienes un cambio de plan pendiente",
		"noDefaultPaymentMethod": "Tienes que tener al menos 1 método de pago adjunto para continuar esta suscripción",
		"cannotPreviewFree": "No puedes previsualizar un cambio a un plan gratuito, cancela tu suscripción en su lugar",
		"cannotPreviewNoSubscription": "No puedes previsualizar un cambio sin una suscripción activa",
		"cannotPreviewSamePlan": "No puedes previsualizar un cambio a tu plan actual",
		"cannotChangeAddedMonths": "Actualmente no puedes cambiar de plan ya que has añadido meses a tu suscripción existente. Puedes cambiar tu plan después de {{date}}.",
		"cannotChangeGifted": "Actualmente no puedes cambiar de plan ya que estás usando una suscripción regalada. Puedes cambiar tu plan después de {{date}}."
	},
	"themesApi": {
		"noDeletePermissions": "No tienes permiso para eliminar este tema"
	},
	"userApi": {
		"privateProfile": "El perfil de este usuario es privado",
		"noValidProperties": "No se han proporcionado propiedades válidas",
		"multiLevelSubdomains": "No se admiten subdominios de múltiples niveles. Usa - en vez de .",
		"subdomainsNotAllowed": "No se permiten subdominios en este dominio",
		"maxDomainCount": "Sólo puedes tener hasta {{count}} dominios seleccionados a la vez",
		"minDomainCount": "Tienes que tener por lo menos 2 dominios seleccionados a la vez",
		"invalidDomain": "{{domain}} no es un dominio válido",
		"invalidSubdomainDomain": "No se permiten subdominios en {{domain}}",
		"autoChanceZero": "Los valores proporcionados dan como resultado que las posibilidades de 'Auto' sean del 0%",
		"randomChanceNot100": "Los valores de probabilidad del dominio aleatorio tienen que sumar 100",
		"invalidUrl": "{{property}} tiene que ser un URL HTTP o HTTPS válido",
		"invalidEmbedFields": "Todas las casillas del embed no pueden estar vacías. Completa algunas casillas, o deshabilita los embeds de Discord",
		"noAuthorOrTitle": "Para que Discord haga el embed correctamente, se requiere un título o autor",
		"invalidTimeformat": "El formato de la hora debe ser 12 o 24 horas. ",
		"cropAreaExceedsSize": "El área de recorte no debe exceder el tamaño de la imagen.",
		"usernameChangeUnavailable": "Ya has cambiado tu nombre de usuario en los últimos 7 días",
		"unsupportedLanguage": "El idioma proporcionado no está soportado",
		"invalidConfigType": "El tipo de configuración proporcionado es inválido",
		"unprocessableGif": "No hemos podido procesar este GIF",
		"fileAgeNegative": "File age cannot be a negative value",
		"fileAgeTooBig": "File age cannot be longer than 12 months"
	},
	"userApis": {
		"MFAApi": {
			"mfaNotInitalised": "MFA no ha sido inicializado",
			"mfaNotEnabled": "No tienes MFA habilitado"
		},
		"paymentApi": {
			"atLeastOnePaymentMethodRequired": "Tienes que tener al menos 1 método de pago con una suscripción activa",
			"defaultPaymentMethod": "No puedes eliminar un método de pago que está marcado como predeterminado",
			"amountDecimalPlaces": "La cantidad no puede tener más de 2 lugares decimales",
			"amountTooSmall": "La cantidad no puede ser menor que {{amount}}",
			"amountTooLarge": "La cantidad no puede ser mayor que {{amount}}"
		},
		"uploadPresetsApi": {
			"missingParameters": "Faltan parámetros en tu solicitud",
			"maxPresets": "Sólo puedes tener hasta 10 ajustes de subida preestablecidos al mismo tiempo"
		}
	}
}
